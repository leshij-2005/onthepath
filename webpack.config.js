const path = require('path');
const webpack = require('webpack');
 
module.exports = {
  entry: [
    'react-hot-loader/patch',
    './src/index.js',
  ],
  output:{
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js',
  },
  externals: {
    yMaps: 'ymaps',
  },
  module:{
    rules:[
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options:{
            presets:['@babel/preset-env', '@babel/preset-react']
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'app.css'
            }
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: 'css-loader',
            options: {
              minimize: true
            }
          },
          {
            loader: 'postcss-loader'
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.(png|jpg|svg|gif|ttf|eot|woff|woff2)$/,
        use: ['file-loader?name=./images/[name].[ext]']
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    hot: true,
    port: 9000,
    proxy: {
      '/route/api/**': {
        target: 'https://na-puti.ru/',
        secure: false,
        changeOrigin: true,
      }
    }
  }
};