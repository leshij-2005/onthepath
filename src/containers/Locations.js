import { connect } from 'react-redux';
import Locations from '../components/locations';

import { syncMap, map } from '../entity/Map';

import { selectLocation, selectWeather, fetchHotels } from '../actions';

const mapStateToProps = ({ locations, weather }) => ({
  ...locations,
  ...weather
});

const mapDispatchToProps = (dispatch) => {
  return {
    onSelect: (id) => {
      dispatch(selectLocation(id));

      syncMap(map);
    },
    onWeatherSelect: (id) => dispatch(selectWeather(id)),
    onHotelsGet: (longitude, latitude) => dispatch(fetchHotels(longitude, latitude)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Locations);   