import { connect } from 'react-redux';
import Sidebar from '../components/sidebar';

const mapStateToProps = ({ locations, weather, hotels }) => {
  return {
    showWeather: !!Object.keys(weather.selected).length, 
    showLocations: !!locations.items.length,
    showHotels: !!hotels.items.length,
  }
}

export default connect(mapStateToProps)(Sidebar);   