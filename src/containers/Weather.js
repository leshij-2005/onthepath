import { connect } from 'react-redux';
import Weather from '../components/weather';

import { selectForecastDate, unSelectWeather } from '../actions';

import { syncMap, map } from '../entity/Map';

const mapStateToProps = ({ weather }) => {
  return {
    weather: weather.selected,
    selectedForecastDate: weather.selectedForecastDate,
    fetching: weather.fetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSelectForecastDate: (date) => dispatch(selectForecastDate(date)),
    onUnselectWeather: () => {
      syncMap(map);

      dispatch(unSelectWeather());
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Weather);   