import { connect } from 'react-redux';

import MapComponent from '../components/map';

const mapStateToProps = ({ stateApp }) => {
  return {
    load: stateApp.load,
    message: stateApp.message,
  }
}

export default connect(mapStateToProps)(MapComponent);   