import { connect } from 'react-redux';
import Hotels from '../components/hotels';

import { selectHotel, unSelectHotel, sortHotels, roomsOpen, photosOpen, clearHotels } from '../actions';

import { syncMap, map } from '../entity/Map';

const mapStateToProps = ({ hotels, locations }) => {
  return Object.assign({}, hotels, { fetchingHotels: locations.fetching });
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSelect: id => {
      dispatch(selectHotel(id));

      syncMap(map);
    },
    onUnSelect: () => {
      dispatch(unSelectHotel());

      syncMap(map);
    },
    onSort: sortBy => dispatch(sortHotels(sortBy)),
    onRoomsOpen: () => dispatch(roomsOpen()),
    onPhotosOpen: () => dispatch(photosOpen()),
    onHideHotels: () => {
      dispatch(clearHotels());

      syncMap(map);
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Hotels);   