import store from '../store';
import { setAppState } from '../actions';

export default class Router {

  constructor({ map, onInit }) {
    this._map = map;
    this._start = null;
    this._finish = null;
    this._route = null;
    this._locations = {};
    this._onInit = onInit;
  }

  _onDragEnd() {
    this.getDirection();
  }

  getDistanceBetweenPoints(from, to) {
    const rad2deg = angle => angle * 57.29577951308232;
    const deg2rad = angle => angle * 0.017453292519943295;

    const theta = from[1] - to[1];
    let distance = Math.sin(deg2rad(from[0])) * Math.sin(deg2rad(to[0])) + Math.cos(deg2rad(from[0])) * Math.cos(deg2rad(to[0])) * Math.cos(deg2rad(theta));

    distance = rad2deg(Math.acos(distance));

    return Math.round(distance * 60 * 1.1515 * 1.609344, 2); 
  }

  filterPointsByDistance(coords) {
    const points= [];

    let prevPoint = this._start.geometry.getCoordinates();
    points.push(prevPoint);

    for (let i = 1; i < coords.length; i++) {
      if (this.getDistanceBetweenPoints(prevPoint, coords[i]) > 5) {
        prevPoint = coords[i];
        points.push(prevPoint);
      }
    }

    return points;
  }

  addRouteToMap(route) {
    this._route = route;
    this._map.geoObjects.add(route);

    this._map.setBounds(this._map.geoObjects.getBounds());
  }

  getDirection() {
    if (this._route) {
      this._map.geoObjects.remove(this._route);
      this._locations = {};      
    }

    if (this._start && this._finish) {
      store.dispatch(setAppState(true, 'Построение маршрута...'));

      const local_coords_id = [...this._start.geometry.getCoordinates(), ...this._finish.geometry.getCoordinates()].join('_');
      const local_coords = localStorage.getItem(local_coords_id);

      if (local_coords) {
        const coords = JSON.parse(local_coords);

        this.addRouteToMap(new ymaps.Polyline(coords, null, { strokeWidth: 5 }));

        this._onInit(this.filterPointsByDistance(coords));

        return;
      }
      
      ymaps
        .route([this._start.geometry.getCoordinates(), this._finish.geometry.getCoordinates()])
        .then(route => {
          this.addRouteToMap(route);
          
          /*path.distance = route.getLength(),
          path.duration = route.getJamsTime();*/

          const paths = route.getPaths();
          const coords = [];

          //
          // coords collection
          //
          
          paths.each(path => {
            const segments = path.getSegments();            

            segments.forEach(segment => {              
              coords.push(...segment.getCoordinates());
            });
          });

          localStorage.setItem(local_coords_id, JSON.stringify(coords));

          this._onInit(this.filterPointsByDistance(coords));
        });
    }
  }

  setPoint(type, point) {
    var position = point.geometry.getCoordinates();
    
    if (this['_' + type]) {
      this['_' + type].geometry.setCoordinates(position);
    } else {
      this['_' + type] = new ymaps.Placemark(position, { iconContent: type ==  'start' ? 'А' : 'B' }, { draggable: true });
      this['_' + type].point = point;
      this['_' + type].events.add('dragend', this._onDragEnd, this);
      this._map.geoObjects.add(this['_' + type]);
    }
    
    this.getDirection();
  }
}