import yMaps from 'yMaps';

import store from '../store';
import { fetchLocations } from '../actions';

import Router from '../entity/Router';
import Weather from '../entity/Weather';

export let map;

export const syncMap = (map) => {
  clearTimeout(map.timeout);

  map.timeout = setTimeout(() => {
    map.setBounds(map.geoObjects.getBounds());
    map.container.fitToViewport();
  }, 500);
}

export function initYMaps() {
  map = new yMaps.Map('map', {
    center: window.GEO ? [GEO['lat'], GEO['lon']] : [55.75399399999374, 37.62209300000001],
    zoom: 9,
    type: 'yandex#map',
    controls: []
  });

  const searchStartPoint = new yMaps.control.SearchControl({
    options: {
      noSuggestPanel: false,
      noPopup: true,
      noPlacemark: true,
      size: 'large',
      placeholderContent: 'Адрес начальной точки',
      maxWidth: 280
    }
  });

  const searchFinishPoint = new yMaps.control.SearchControl({
    options: {
      noSuggestPanel: false,
      noPopup: true,
      noPlacemark: true,
      placeholderContent: 'Адрес конечной точки',
      float: 'none',
      size: 'large',
      maxWidth: 280,
      position: { left: 10, top: 44 }
    }
  });

  searchStartPoint.events.add('load', event => {
    if (!event.get('skip') && searchStartPoint.getResultsCount()) {
      const results = searchStartPoint.getResultsArray();
      router.setPoint('start', results[0]);

      const locationName = results[0].properties.get('name');
      setLocationsPath('locationFrom', locationName);
    }
  });

  searchFinishPoint.events.add('load', event => {
    if (!event.get('skip') && searchFinishPoint.getResultsCount()) {
      const results = searchFinishPoint.getResultsArray();
      router.setPoint('finish', results[0]);

      const locationName = results[0].properties.get('name');
      setLocationsPath('locationTo', locationName);
    }
  });  

  map.controls.add(searchStartPoint);
  map.controls.add(searchFinishPoint);

  const weather = new Weather(map);
  const router = new Router({ map, onInit: (points) => {
    store
      .dispatch(fetchLocations(points))
      .then(({ items }) => {
        weather.get(items);
      });
  }});

  const getLocationsPath = () => {
    const paths = (location.hostname == 'localhost' ? location.hash.replace('#/', '') : location.pathname.replace('/iframe', '')).split('/');
    const locations = {}
    
    if (!paths[0] || paths[0] === 'iframe') {
      paths.shift();
    }

    if (!paths[paths.length - 1]) {
      paths.pop();
    }
    
    if (paths.length > 1) {
      locations.locationFrom = decodeURI(paths[1]);
    }

    if (paths.length > 2) {
      locations.locationTo = decodeURI(paths[paths.length - 1]);
    }

    return locations;
  }

  const setLocationsPath = (key, value) => {
    const paths = ['route'];
    const locations = getLocationsPath();

    locations[key] = value;

    for(let key in locations) {
      paths.push(locations[key]);
    }

    let path = '/' + paths.join('/') + '/';

    if (location.hostname == 'localhost') {
      const hashIndex = window.location.href.indexOf('#');
      path = (hashIndex != -1 ? window.location.href.substr(0, hashIndex) : window.location.href) + '#' + path;
    }

    history.pushState({}, document.title, path);
  }

  if (location.pathname) {
    const locations = getLocationsPath();

    if (locations) {
      searchStartPoint.search(locations.locationFrom);
      searchFinishPoint.search(locations.locationTo);
    }
  }
}