import yMaps from 'yMaps';

import store from '../store';

import { fetchWeather, receiveLocations } from '../actions';

class Weather {
  constructor(map) {
    this._placemarks = new yMaps.GeoObjectCollection();
    this._map = map;
  }

  get(locations = []) {
    this._placemarks.removeAll();
    
    const hasWeather = {};

    locations.forEach(loc => {
      if (loc.weatherid) {
        hasWeather[loc.id] = loc;
      }
    });
    
    store
      .dispatch(fetchWeather(Object.keys(hasWeather).slice(0, 20).join(',')))
      .then(({ list }) => {
        Object.keys(list).forEach(id => {
          const weather = list[id];
          
          const placemark =  new yMaps.Placemark([weather.coord.lat, weather.coord.lon], {
              hintContent: weather.description
            },
            {
              iconLayout: 'default#image',
              iconImageHref: '//openweathermap.org/img/w/' + weather.icon + '.png',
            }
          );

          this._placemarks.add(placemark);
        });

        this._map.geoObjects.add(this._placemarks);
      });
  }
}

Weather.reader = (data) => {
  let weather = data.weather[0];
  
  weather = Object.assign(weather, data.main);
  weather.coord = data.coord;
  weather.wind = data.wind;
  weather.sys = data.sys;
  weather.rain = data.rain && data.rain['3h'];
  weather.snow = data.snow && data.snow['3h'];
  weather.cityId = data.id;

  return weather;
}

export default Weather;