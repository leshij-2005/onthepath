import React from 'react';
import PropTypes from 'prop-types';

import Locations from '../containers/Locations';
import Weather from '../containers/Weather';
import Hotels from '../containers/Hotels';

const Sidebar = ({ showLocations, showWeather, showHotels }) => {
  let extClass = '';

  extClass = extClass + (showLocations ? ' sidebar_locations' : '');
  extClass = extClass + (showWeather ? ' sidebar_weather' : '');
  extClass = extClass + (showHotels && !showWeather ? ' sidebar_hotels' : '');

  return (
    <div className={'sidebar' + extClass}>
      <div className="sidebar-block sidebar-block_locations">
        <Locations />
      </div>
      <div className="sidebar-block sidebar-block_hotels">
        <Hotels />
      </div>
      <div className="sidebar-block sidebar-block_weather">
        <Weather />        
      </div>
    </div>
  );
};

Sidebar.propTypes = {
  showLocations: PropTypes.bool.isRequired,
  showWeather: PropTypes.bool.isRequired,
  showHotels: PropTypes.bool.isRequired,
};

export default Sidebar;