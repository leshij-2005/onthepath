import React from 'react';

import STATIC from '../utils/static.js';

const DETAILS_PARAMS_MAP = {
  rating: 'Рейтинг посетителей',
  //popularity: 'Популярность отеля',
  propertyType: 'Тип отеля',
  checkOut: 'Время выселения',
  checkIn: 'Время заселения',
  distance: 'Расстояние до центра, км',
  cntFloors: 'Количество этажей',
  cntRooms: 'Количество комнат',
  address: 'Адрес отеля',
  minPriceTotal: 'Цена за номер от, руб'
};

const Star = () => (<span></span>);

const Param = ({ data }) => (
  <div className="hotels-item-details-param">
    <div className="hotels-item-details-param__ttl">{data.title}</div>
    <div className="hotels-item-details-param__val">{data.value}</div>
  </div>
);

const Photo = ({ url }) => (<div className="hotels-item-details-photo"><span style={{backgroundImage: 'url("' + url + '")'}}></span></div>);

const Room = ({ data }) => (
  <div className="hotels-item-details-room">
    <div className="hotels-item-details-room-booking">
      <div className="hotels-item-details-room__price">{data.price} руб/сутки</div>
      <a href={data.fullBookingURL} target="_blank" className="button button_small">Забронировать</a>
    </div>
    <div className="hotels-item-details-room__desc">{data.desc}</div>
  </div>
);


const Hotel = ({ data, selected, roomsOpened, photosOpened, fetching, onRoomsOpen, onPhotosOpen, onSelect, onUnSelect }) => {
  const stars = [];
  const params = [];
  const photos = [];
  const rooms = [];

  for (let i = 0; i < data.stars; i++) {
    stars.push(<Star key={i}/>);
  }

  Object.keys(DETAILS_PARAMS_MAP).forEach(name => {
    const title = DETAILS_PARAMS_MAP[name];

    if (data[name]) {
      const obj = {
        title: title,
        value: typeof data[name] == 'object' ? (data[name].ru || data[name].en) : data[name]
      }

      if (name == 'propertyType') {
        obj.value = STATIC.hotelTypes[params.value];
      }

      if (name == 'rating') {
        obj.value = obj.value + '/100';
      }

      params.push(
        <Param key={name} data={obj}/>
      );
    }
  });

  data.photos.forEach((photo, idx) => {
    photos.push(
      <Photo key={idx} url={photo.url}/>
    );
  });

  const link = `http://travel.ticket-on.ru/hotels${data.link}`;

  if (data.rooms) {
    data.rooms.forEach((room, idx) => {
      rooms.push(
        <Room key={idx} data={room}/>
      );
    });
  }

  return (      
    <div className={'hotels-item' + (selected ? ' hotels-item_selected' : '')} onClick={() => onSelect(data.id)}>
      <div className="hotels-item__back"><a onClick={e => { e.stopPropagation(); onUnSelect(); }}></a></div>
      <div className="hotels-item__info"><a title="Подробнее" onClick={e => { e.stopPropagation(); onSelect(data.id); }}></a></div>
      <div className="hotels-item__name">{data.name.ru || data.name.en}</div>
      <div className="hotels-item__stars">{stars}</div>
      {selected && 
        <div className={'hotels-item-details ' + (fetching && 'processing')} onClick={e => e.stopPropagation()}>
          <div className="hotels-item-details-params">{params}</div>
          <div className="hotels-item-details__link">
            <a href={link} target="_blank" className="button">Страница отеля</a>
          </div>
          <div className={'hotels-item-details-rooms' + (roomsOpened ? ' hotels-item-details-rooms_opened' : '')} style={{display: rooms.length ? null : 'none'}}>
            <div className="hotels-item-details-rooms__ttl">Доступные номера</div>
            {rooms}
            <div className="hotels-item-details-rooms__open" onClick={() => onRoomsOpen()}></div>
          </div>
          <div className={'hotels-item-details-photos' + (photosOpened ? ' hotels-item-details-photos_opened' : '')} style={{display: photos.length ? null : 'none'}}>
            <div className="hotels-item-details-photos__ttl">Фотографии номеров</div>
            {photos}
            <div className="hotels-item-details-photos__open" onClick={() => onPhotosOpen()}></div>
          </div>
        </div>
      }
    </div>
  );
}

export default Hotel;