import React from 'react';
import PropTypes from 'prop-types';

const Map = ({ load, message }) => ( 
  <div className="map">
    <div id="map"></div>
    <div className={'map-loading' + (load ? ' map-loading_load' : '')}>
      <div className="map-loading__label">{message}</div>
    </div>
  </div> 
);

Map.propTypes = {
  load: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
};

export default Map;