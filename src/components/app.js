import React, { Component } from 'react';

import { initYMaps } from '../entity/Map';

import Map from '../containers/map';
import Sidebar from '../containers/sidebar';

export default class App extends Component {
  componentDidMount() {
    initYMaps();
  }

  render() {
    return (
        <div className="main">
          <Map />
          <Sidebar />
        </div>
    );
  }
}