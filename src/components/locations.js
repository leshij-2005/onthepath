import React, { Component } from 'react';

class Location extends Component {
  getHotels() {
    const { onHotelsGet, data } = this.props;

    onHotelsGet(data.longitude, data.latitude);

    this.select();
  }

  select() {
    const { onSelect, data } = this.props;

    onSelect(data.id);
  }

  showWeather() {
    const { onWeatherSelect, data } = this.props;

    onWeatherSelect(data.id);

    this.select();
  }

  render() {
    const { data, selected, fetching, weather } = this.props;
    const weatherImgURL = weather ? `//openweathermap.org/img/w/${weather.icon}.png` : '';

    return (      
      <div className={'locations-item' + (selected ? ' locations-item_selected' : '')}>
        <div className="locations-item__distance"><span>{data.kilometers ? data.kilometers + 'км' : ''}</span></div>
        <div className="locations-item__name">{data.name}</div>
        <div className={'locations-item__hotels ' + (fetching && selected && 'processing')} onClick={() => this.getHotels()} title="Отели"><span></span></div>
        {
          weather         
            ? <div className="locations-item__weather" onClick={() => this.showWeather()} title="Погода"><img src={weatherImgURL} /></div> 
            : <div className="locations-item__weather"></div>
        }
      </div>
    );
  }
}

const Locations = ({ items, list: weather, selected, fetching, onSelect, onWeatherSelect, onHotelsGet }) => {
  const nodes = [];

  if (items) {
    items.forEach(item => {
      nodes.push(
        <Location 
          key={item.id} 
          data={item} 
          selected={selected === item.id}
          fetching={fetching}
          weather={weather[item.id]}
          onSelect={id => onSelect(id)} 
          onWeatherSelect={id => onWeatherSelect(id)} 
          onHotelsGet = {(longitude, latitude) => onHotelsGet(longitude, latitude)} 
        />
      );
    });
  }

  return (
    <div className="locations">
      <div className="locations-items">{nodes}</div>
    </div>
  );
}

export default Locations;