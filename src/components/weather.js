import React, { Component } from 'react';

import Weather from '../entity/Weather';

const MONTHS = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

const getWind = (deg) => {
  let str = '';

  switch(true)
  {
    case deg == 0 || (deg > 315  && deg <= 360):
      str = 'север';
      break;

    case deg > 0 && deg <= 45:
      str = 'северо-восток';
      break;

    case deg > 45 && deg <= 90:
      str = 'восток';
      break;

    case deg > 90 && deg <= 135:
      str = 'юго-восток';
      break;

    case deg > 135 && deg <= 180:
      str = 'юг';
      break;

    case deg > 180 && deg <= 225:
      str = 'юго-запад';
      break;

    case deg > 225 && deg <= 270:
      str = 'запад';
      break;

    case deg > 270 && deg <= 315:
      str = 'северо-запад';
      break;
  }

  return str;
};

const ForecastRow = ({ forecast }) => {
  const data = Weather.reader(forecast);

  const temp = data.temp_min == data.temp_max ? data.temp : data.temp_min + '-' + data.temp_max;
  const wind = getWind(data.wind && data.wind.deg);
  const speed = data.wind && data.wind.speed;
  const icon = `http://openweathermap.org/img/w/${data.icon}.png`;
  const precipitation = data.rain || data.snow;
  const time = forecast.dt_txt.split(' ')[1].split(':');

  return (
    <div className="weather-forecast-group-row">
      <div className="weather-forecast-group-cell">
        <div>{[time[0], time[1]].join(':') + ' МСК'}</div>
        <img src={icon} alt=""/>
      </div>
      <div className="weather-forecast-group-cell">{temp}&deg;</div>
      <div className="weather-forecast-group-cell">{wind ? (speed + ' м/c, ' + wind) : ''}</div>
      <div className="weather-forecast-group-cell">{precipitation ? precipitation + ' м.м за 3 часа' : ''}</div>
    </div>
  );
}

const Forecast = ({ date, forecast, selected, onSelect }) => {
  const rows = [];

  forecast.forEach((data, idx) => {
    rows.push(<ForecastRow key={idx} forecast={data} />);
  });

  return (
    <div className={'weather-forecast-group' + (selected ? ' weather-forecast-group_selected' : '')}>
      <div className="weather-forecast-group__date" onClick={() => onSelect(date)}>{date}</div>
      <div className="weather-forecast-group-rows">
        <div className="weather-forecast-group-row">
          <div className="weather-forecast-group-cell"></div>
          <div className="weather-forecast-group-cell">Температура</div>
          <div className="weather-forecast-group-cell">Ветер</div>
          <div className="weather-forecast-group-cell">Осадки</div>
        </div>
        {rows}
      </div>
    </div>
  );
}

const WeatherComponent = ({ weather: data = {}, selectedForecastDate, fetching, onSelectForecastDate, onUnselectWeather }) => {
  const temp = data.temp_min == data.temp_max ? data.temp : data.temp_min + '-' + data.temp_max;
  const wind = getWind(data.wind && data.wind.deg);
  const speed = data.wind && data.wind.speed;
  const icon = data.icon ? `http://openweathermap.org/img/w/${data.icon}.png` : '';

  const forecastItems = [];
  const forecast = {};

  if (data.forecast) {
    const today = new Date();
    const today_str = [today.getDate(), MONTHS[today.getMonth()], today.getFullYear()].join(' ');

    data.forecast.forEach(item => {
      const date = item.dt_txt.split(' ')[0].split('-');
      const date_str = [date[2], MONTHS[parseInt(date[1]) - 1], date[0]].join(' ');

      if (!forecast[date_str]) {
        forecast[date_str] = [];
      }

      forecast[date_str].push(item); 
    });

    Object.keys(forecast).forEach(date => {
      const list = forecast[date];

      const selected = selectedForecastDate ? selectedForecastDate == date : date == today_str;

      forecastItems.push(<Forecast key={date} date={date} forecast={list} selected={selected} onSelect={date => onSelectForecastDate(date)}/>);
    });
  }

  return (
    <div className="weather">
      <div className="weather-header">
        <a className="weather-header__back" onClick={() => onUnselectWeather()}></a>
        <div className="weather-header__ttl">Погода{data.description ? (' - ' + data.description) : ''}</div>
      </div>
      <div className="weather-details">
        <div className="weather-details-header">
          <div className="weather-details__icon">{icon ? <img src={icon} alt=""/> : ''}</div>
          <div className="weather-details__temp">{temp}&deg;</div>
        </div>
        <div className="weather-params">
          <div className="weather-param">
            <div className="weather-param__ttl">Ветер</div>
            <div className="weather-param__val">{speed} м/c, {wind}</div>
          </div>
          {
            data.rain ?
              <div className="weather-param">
                <div className="weather-param__ttl">Дождь</div>
                <div className="weather-param__val">{data.rain} м.м за 3 часа</div>
              </div>
            : ''
          }
          {
            data.snow ?
              <div className="weather-param">
                <div className="weather-param__ttl">Снег</div>
                <div className="weather-param__val">{data.snow} м.м за 3 часа</div>
              </div>
            : ''
          }
        </div>
      </div>
      <div className={'weather-forecast ' + (fetching && 'processing')}>
        <div className="weather-forecast__title">Прогноз</div>
        {forecastItems}
      </div>
    </div>
  );
}

export default WeatherComponent;