import React from 'react';

import Hotel from './hotel';

const HotelsSortingItem = ({ data, selected, onSelect }) => {
  return (
    <a className={'hotels-sorting-item' + (selected ? ' hotels-sorting-item_selected' : '')} onClick={() => onSelect(data.name)}>{data.title}</a>
  );
}

const HotelsSorting = ({ selected, onSelect, onHideHotels }) => {
  const items = [
    {
      name: 'popularity', 
      title: 'По популярности',
    },
    {
      name: 'rating', 
      title: 'По рейтингу'
    },
    {
      name: 'stars', 
      title: 'По звездам'
    }
  ];

  const nodes = [];


  items.forEach((item, idx) => {
    nodes.push(
      <HotelsSortingItem key={idx} selected={selected == item.name} data={item} onSelect={name => onSelect(name)}/>
    );
  });

  return (
    <div className="hotels-sorting">
      <a className="hotels-sorting__back" onClick={() => onHideHotels()}></a>
      {nodes}
    </div>
  );
}

const Hotels = ({ items, selected, sortBy, roomsOpened, photosOpened, fetching, fetchingHotels, onHideHotels, onRoomsOpen, onPhotosOpen, onSelect, onUnSelect,  onSort}) => {
  const nodes = [];

  if (sortBy) {
    items.sort((a, b) => {
      return a[sortBy] < b[sortBy] ? 1 : -1;
    });
  }

  if (items) {
    items.forEach((item, idx) => {
      nodes.push(
        <Hotel
          key={idx}
          selected={selected === item.id}
          data={item}
          onSelect={id => onSelect(id)}
          onUnSelect={() => onUnSelect()}
          roomsOpened={roomsOpened}
          photosOpened={photosOpened}
          fetching={fetching}
          onRoomsOpen={() => onRoomsOpen()}
          onPhotosOpen={() => onPhotosOpen()} 
        />
      );
    });
  }

  return (
    <div className={'hotels ' + (fetchingHotels && 'processing')}>
      <HotelsSorting selected={sortBy} onSelect={name => onSort(name)} onHideHotels={() => onHideHotels()}/>
      <div className="hotels-wrap">
        <div className="hotels-items">{nodes}</div>
      </div>
    </div>
  );
}

export default Hotels;