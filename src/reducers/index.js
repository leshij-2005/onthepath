import { combineReducers } from 'redux';

import stateApp from './stateApp';
import locations from './locations';
import weather from './weather';
import hotels from './hotels';

const reducers = combineReducers({
  stateApp,
  locations,
  weather,
  hotels
});

export default reducers;