import { RECEIVE_WEATHER, SELECT_WEATHER, UNSELECT_WEATHER, RECEIVE_WEATHER_FORECAST, SELECT_FORECAST_DATE, FETCHING_WEATHER_FORECAST } from '../actions';

const initialState = {
  list: [],
  selected: {},
  selectedForecastDate: null,
  fetching: false,
};

function weather(state = initialState, action) {
  switch (action.type) {
    case RECEIVE_WEATHER:
      return Object.assign({}, state, {
        list: action.list,
      });

    case SELECT_WEATHER:
      return Object.assign({}, state, {
        selected: state.list[action.id],
        selectedForecastDate: null,
      });

    case UNSELECT_WEATHER:
      return Object.assign({}, state, {
        selected: {},
        selectedForecastDate: null,
      });

    case RECEIVE_WEATHER_FORECAST:
      return Object.assign({}, state, {
        selected: Object.assign({}, state.selected, { forecast: action.list }),
      });

    case SELECT_FORECAST_DATE:
      return Object.assign({}, state, {
        selectedForecastDate: action.date,
      }); 
    
    case FETCHING_WEATHER_FORECAST:
      return Object.assign({}, state, {
        fetching: action.fetching,
      }); 

    default:
      return state;
  }
}

export default weather;