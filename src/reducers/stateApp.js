import { SET_APP_STATE } from '../actions';

const initialState = {
  load: false,
  message: '',
};

function stateApp(state = initialState, action) {
  switch (action.type) {
    case SET_APP_STATE: 
      return Object.assign({}, state, {
        load: action.load,
        message: action.message,
      });

    default:
      return state;
  }
}

export default stateApp;