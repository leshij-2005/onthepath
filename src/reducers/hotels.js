import { 
  RECEIVE_HOTELS,
  SELECT_HOTEL,
  UNSELECT_HOTEL,
  SORT_HOTELS, 
  RECEIVE_HOTEL_BOOKING, 
  HOTEL_PHOTOS_OPEN, 
  HOTEL_ROOMS_OPEN,
  FETCHING_HOTEL
} from '../actions';

const initialState = {
  items: [],
  selected: null,
  sortBy: 'popularity',
  roomsOpened: false,
  photosOpened: false,
  fetching: false,
  fetchingHotels: false,
};

function hotels(state = initialState, action) {
  switch (action.type) {
    case SORT_HOTELS: 
      return Object.assign({}, state, {
        sortBy: action.sortBy,
      });

    case RECEIVE_HOTELS: 
      return Object.assign({}, state, {
        items: action.items,
        sortBy: 'popularity',
      });

    case SELECT_HOTEL: 
      return Object.assign({}, state, {
        selected: action.id,
        roomsOpened: false,
        photosOpened: false,
      });    
      
    case UNSELECT_HOTEL:
      return Object.assign({}, state, {
        selected: null,
      });

    case FETCHING_HOTEL:
      return Object.assign({}, state, {
        fetching: action.fetching,
      });
      
    case RECEIVE_HOTEL_BOOKING:
      const { minPriceTotal, amenities, rooms } = action;

      const items = state.items.map(item => {
        if (item.id === state.selected) {
          return Object.assign({}, item, { minPriceTotal, amenities, rooms });
        }

        return item;
      });

      return Object.assign({}, state, {
        items,
      });
    
    case HOTEL_PHOTOS_OPEN:
      return Object.assign({}, state, {
        photosOpened: true,
      });

    case HOTEL_ROOMS_OPEN:
      return Object.assign({}, state, {
        roomsOpened: true,
      });      
      
    default:
      return state;
  }
}

export default hotels;