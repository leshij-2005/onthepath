import { RECEIVE_LOCATIONS, SELECT_LOCATION, FETCHING_LOCATION } from '../actions';

const initialState = {
  items: [],
  selected: null,
  fetching: false,
};

function locations(state = initialState, action) {
  switch (action.type) {
    case RECEIVE_LOCATIONS:
      return Object.assign({}, state, {
        items: action.items,
      });

    case SELECT_LOCATION: 
      return Object.assign({}, state, {
        selected: action.id,
      });

    case FETCHING_LOCATION: 
      return Object.assign({}, state, {
        fetching: action.fetching,
      });

    default:
      return state;
  }
}

export default locations;