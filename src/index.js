import { polyfill } from 'es6-promise';

polyfill();

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import yMaps from 'yMaps';

import store from './store';
import App from './components/app';

import './styles/app.scss';

yMaps.ready(() => {
  render(
    <Provider store={store}>
      <App />
    </Provider>, 
    document.getElementById('app')
  );
});

module.hot.accept();