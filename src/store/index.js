import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux';

import reducers from '../reducers';

const store = createStore(reducers, applyMiddleware(
  thunkMiddleware
));

// console.log(store.getState());

export default store;