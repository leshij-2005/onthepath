import fetch from 'isomorphic-fetch'
import Weather from '../entity/Weather'

export const SET_APP_STATE = 'SET_APP_STATE';
export function setAppState(load = false, message = '') {
  return { type: SET_APP_STATE, load, message };
}

export const RECEIVE_LOCATIONS = 'RECEIVE_LOCATIONS';
export function receiveLocations(items) {
  return { type: RECEIVE_LOCATIONS, items };
}

export const fetchLocations = (points) => {
  return dispatch => {
    return fetch('/route/api/locations/get.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ points }),
    })
    .then(response => response.json())
    .then(items => dispatch(receiveLocations(items)))
  }
}

export const SELECT_LOCATION = 'SELECT_LOCATION';
export function selectLocation(id) {
  return dispatch => {
    dispatch({ type: SELECT_LOCATION, id });
    dispatch(clearHotels());
  }
}

export const FETCHING_LOCATION = 'FETCHING_LOCATION';
export function fetchingLocation(fetching) {  
  return { type: FETCHING_LOCATION, fetching };
}

export const RECEIVE_WEATHER = 'RECEIVE_WEATHER';
export function receiveWeather(list) {
  return { type: RECEIVE_WEATHER, list };
}

export const fetchWeather = (ids) => {
  return dispatch => {
    dispatch(fetchingLocation(true));
    dispatch(setAppState(true, 'Загрузка погоды..'));

    const url = new URL('https://api.openweathermap.org/data/2.5/group');
    const params = {
      id: ids,
      appid: '598ec5533e9e18863860f636feb8ccd6',
      units: 'metric',
      lang: 'ru'
    }

    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

    return fetch(url)
    .then(response => response.json())
    .then(({ list }) => {
      dispatch(setAppState());
      dispatch(fetchingLocation(false));

      const items = {};

      list.forEach(item => {
        items[item.id] = Weather.reader(item);
      });

      return dispatch(receiveWeather(items));
    });
  }
}

export const SELECT_WEATHER = 'SELECT_WEATHER';
export function selectWeather(id) {
  return dispatch => {
    dispatch({ type: SELECT_WEATHER, id });

    dispatch(fetchWeatherForecast(id));
  }
}

export const UNSELECT_WEATHER = 'UNSELECT_WEATHER';
export function unSelectWeather() {
  return { type: UNSELECT_WEATHER };
}

export const RECEIVE_WEATHER_FORECAST = 'RECEIVE_WEATHER_FORECAST';
export function receiveWeatherForecast(list) {
  return { type: RECEIVE_WEATHER_FORECAST, list };
}

export const FETCHING_WEATHER_FORECAST = 'FETCHING_WEATHER_FORECAST';
export function fetchingWeatherForecast(fetching) {
  return { type: FETCHING_WEATHER_FORECAST, fetching };
}

export const fetchWeatherForecast = (id) => {
  return dispatch => {
    dispatch(fetchingWeatherForecast(true));

    const url = new URL('https://api.openweathermap.org/data/2.5/forecast');
    const params = {
      id,
      appid: '598ec5533e9e18863860f636feb8ccd6',
      units: 'metric',
      lang: 'ru'
    }

    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

    return fetch(url)
    .then(response => response.json())
    .then(({ list }) => {
      dispatch(fetchingWeatherForecast(false));

      return dispatch(receiveWeatherForecast(list));
    });
  }
}

export const SELECT_FORECAST_DATE = 'SELECT_FORECAST_DATE';
export function selectForecastDate(date) {
  return { type: SELECT_FORECAST_DATE, date };
}

export const RECEIVE_HOTELS = 'RECEIVE_HOTELS';
export function receiveHotels(items) {  
  return { type: RECEIVE_HOTELS, items };
}

export function clearHotels() {  
  return { type: RECEIVE_HOTELS, items: [] };
}

export const fetchHotels = (longitude, latitude) => {
  return dispatch => {
    const url = `/route/api/hotels/get.php?lon=${longitude}&lat=${latitude}`;

    dispatch(fetchingLocation(true));

    return fetch(url)
    .then(response => response.json())
    .then(({ hotels }) => {
      dispatch(receiveHotels(hotels));

      dispatch(unSelectWeather());
      
      dispatch(fetchingLocation(false));
    })
  }
}

export const RECEIVE_HOTEL_BOOKING = 'RECEIVE_HOTEL_BOOKING';
export function receiveHotelBooking(minPriceTotal, amenities, rooms) {  
  return { type: RECEIVE_HOTEL_BOOKING, minPriceTotal, amenities, rooms };
}

export const fetchHotelBooking = (id) => {
  return dispatch => {
    const url = `/route/api/hotels/booking.php?hotelId=${id}`;

    dispatch(fetchingHotel(true));

    return fetch(url)
    .then(response => response.json())
    .then(({ minPriceTotal, amenities, rooms }) => {
      dispatch(receiveHotelBooking(minPriceTotal, amenities, rooms));

      dispatch(fetchingHotel(false));
    })
  }
}

export const FETCHING_HOTEL = 'FETCHING_HOTEL';
export function fetchingHotel(fetching) {
  return { type: FETCHING_HOTEL, fetching };
}

export const SELECT_HOTEL = 'SELECT_HOTEL';
export function selectHotel(id) {
  return dispatch => {
    dispatch({ type: SELECT_HOTEL, id });

    dispatch(fetchHotelBooking(id));
  }
}

export const UNSELECT_HOTEL = 'UNSELECT_HOTEL';
export function unSelectHotel() {
  return { type: UNSELECT_HOTEL };
}

export const SORT_HOTELS = 'SORT_HOTELS';
export function sortHotels(sortBy) {
  return { type: SORT_HOTELS, sortBy };
}

export const HOTEL_ROOMS_OPEN = 'HOTEL_ROOMS_OPEN';
export function roomsOpen() {
  return { type: HOTEL_ROOMS_OPEN };
}

export const HOTEL_PHOTOS_OPEN = 'HOTEL_PHOTOS_OPEN';
export function photosOpen() {
  return { type: HOTEL_PHOTOS_OPEN };
}