const data = {
  "roomTypes":{
    "1":"Standard room",
    "2":"Superior room",
    "3":"Triple room",
    "4":"Deluxe room",
    "5":"Studio",
    "6":"Economy room",
    "7":"Junior suite",
    "8":"Apartment",
    "9":"Suite",
    "10":"Comfort room",
    "11":"Executive room",
    "12":"Family room",
    "13":"Quadruple room",
    "14":"Deluxe apartment",
    "15":"Business room",
    "16":"Club room",
    "17":"Villa",
    "18":"Bungalow",
    "19":"Premium room",
    "20":"Superior suite",
    "21":"Studio suite",
    "22":"Cottage",
    "23":"Premier room",
    "24":"Deluxe triple room",
    "25":"Executive suite",
    "26":"Deluxe villa",
    "27":"Luxury room",
    "28":"Deluxe bungalow",
    "29":"Junior suite apartment",
    "30":"Deluxe family room",
    "31":"Dorm",
    "32":"Superior single room",
    "33":"Single deluxe room",
    "34":"Deluxe king room",
    "35":"Single room",
    "36":"Single suite",
    "37":"Single junior suite",
    "38":"Family suite",
    "39":"Deluxe suite",
    "40":"President suite",
    "41":"Deluxe cottage",
    "42":"Premium suite",
    "43":"Super deluxe room",
    "44":"Loft",
    "45":"Superior triple room",
    "46":"Superior cottage",
    "47":"Suite apartment",
    "48":"Deluxe studio",
    "49":"Superior studio",
    "50":"Superior quadruple room",
    "51":"Superior apartment",
    "52":"Executive apartment",
    "54":"Superior bungalow",
    "55":"Deluxe dorm",
    "56":"Superior dorm",
    "57":"Super suite",
    "58":"Economy quadruple room",
    "59":"Executive family room",
    "60":"Economy apartment",
    "61":"Superior family room",
    "62":"Comfort apartment",
    "64":"Single economy room",
    "65":"Single executive room",
    "66":"Single comfort room",
    "67":"Premium apartment",
    "68":"Superior deluxe room",
    "69":"Single executive suite",
    "70":"Single business room",
    "71":"Vip room",
    "72":"Business suite",
    "73":"Business apartment",
    "74":"Economy triple room",
    "75":"King room",
    "76":"President room",
    "77":"Premier apartment",
    "78":"Premier suite",
    "79":"Business superior room",
    "80":"President villa",
    "81":"Economy family room",
    "82":"President junior suite",
    "83":"Club apartment",
    "84":"Superior queen room",
    "85":"Executive club room",
    "86":"Business deluxe room",
    "87":"Business executive room",
    "88":"President apartment",
    "89":"Vip apartment",
    "90":"King apartment",
    "91":"President suite villa",
    "92":"Executive suite apartment",
    "128":"Executive king room",
    "129":"King executive suite",
    "130":"King junior suite",
    "131":"King suite",
    "132":"Superior king room",
    "133":"Superior villa",
    "134":"Suite villa",
    "135":"King villa",
    "136":"Executive villa",
    "137":"Executive deluxe room",
    "138":"Club deluxe room",
    "139":"Club suite",
    "140":"Privilege room",
    "141":"Chalet",
    "142":"Superior junior suite",
    "143":"Deluxe junior suite",
    "144":"Premier studio",
    "145":"Executive junior suite",
    "146":"Executive studio",
    "147":"Quintuple room",
    "148":"Deluxe chalet",
    "149":"Superior executive room",
    "150":"Luxury single room",
    "151":"Single club room",
    "152":"Family comfort room",
    "153":"Superior club room",
    "154":"House",
    "155":"Premium Deluxe room",
    "156":"Executive triple room",
    "157":"Family Junior Suite",
    "158":"Sextuple room",
    "159":"Economy Sextuple room",
    "160":"Comfort triple room",
    "161":"Luxury Villa",
    "162":"Premium triple room",
    "163":"Premium single room"
  },
  "hotelTypes":{
    "0":"Other",
    "1":"Hotel",
    "2":"Apartment Hotel",
    "3":"Bed & Breakfast",
    "4":"Apartment / Condominium",
    "5":"Motel",
    "6":"Guest House",
    "7":"Hostel",
    "8":"Resort",
    "9":"Farm Stay",
    "10":"Vacation Rental",
    "11":"Lodge",
    "12":"Villa"
  },
  "amenities":[
    {
      "groupName":"В номере",
      "name":"Телевизор",
      "id":"4"
    },
    {
      "groupName":"Основное",
      "name":"Ресторан",
      "id":"9"
    },
    {
      "groupName":"В номере",
      "name":"Кондиционер",
      "id":"11"
    },
    {
      "groupName":"В номере",
      "name":"Мини-бар",
      "id":"16"
    },
    {
      "groupName":"Услуги",
      "name":"Круглосуточное обслуживание в номерах",
      "id":"23"
    },
    {
      "groupName":"Основное",
      "name":"Можно с животными",
      "id":"28"
    },
    {
      "groupName":"Услуги",
      "name":"Услуга \"Будильник\"",
      "id":"30"
    },
    {
      "groupName":"В номере",
      "name":"Гладильная доска",
      "id":"35"
    },
    {
      "groupName":"Услуги",
      "name":"Факс",
      "id":"47"
    },
    {
      "groupName":"Парковка",
      "name":"Бесплатная парковка",
      "id":"54"
    },
    {
      "groupName":"Основное",
      "name":"Условия для лиц с ограниченными возможностями",
      "id":"59"
    },
    {
      "groupName":"В номере",
      "name":"Халаты",
      "id":"61"
    },
    {
      "groupName":"В номере",
      "name":"Холодильник",
      "id":"66"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Теннисный корт",
      "id":"73"
    },
    {
      "groupName":"Услуги",
      "name":"Услуги врача",
      "id":"78"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Игровая площадка",
      "id":"80"
    },
    {
      "groupName":"Основное",
      "name":"Терраса для загара",
      "id":"85"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Настольный теннис",
      "id":"92"
    },
    {
      "groupName":"Основное",
      "name":"Зона для барбекю",
      "id":"97"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Анимация",
      "id":"100"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Ночной клуб",
      "id":"105"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Катание на лошадях",
      "id":"112"
    },
    {
      "groupName":"Основное",
      "name":"Комната для хранения лыж",
      "id":"117"
    },
    {
      "groupName":"Услуги",
      "name":"Бесплатные местные телефонные звонки",
      "id":"124"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Водные виды спорта",
      "id":"129"
    },
    {
      "groupName":"Основное",
      "name":"Wi-Fi в лобби",
      "id":"131"
    },
    {
      "groupName":"Персонал говорит на",
      "name":"Английский",
      "id":"136"
    },
    {
      "groupName":"Персонал говорит на",
      "name":"Русский",
      "id":"143"
    },
    {
      "groupName":"В номере",
      "name":"Телефон",
      "id":"5"
    },
    {
      "groupName":"Основное",
      "name":"Магазины",
      "id":"12"
    },
    {
      "groupName":"Интернет",
      "name":"Доступ в интернет",
      "id":"24"
    },
    {
      "groupName":"Основное",
      "name":"Удобства для лиц с ограниченными возможностями",
      "id":"29"
    },
    {
      "groupName":"В номере",
      "name":"Ежедневная газета",
      "id":"31"
    },
    {
      "groupName":"Основное",
      "name":"Сад",
      "id":"36"
    },
    {
      "groupName":"Основное",
      "name":"Ресепшн",
      "id":"43"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Массаж",
      "id":"48"
    },
    {
      "groupName":"Основное",
      "name":"Ресепшн 24 часа",
      "id":"50"
    },
    {
      "groupName":"В номере",
      "name":"Просмотр фильмов в номере",
      "id":"62"
    },
    {
      "groupName":"В номере",
      "name":"Детская кроватка",
      "id":"67"
    },
    {
      "groupName":"Услуги",
      "name":"Медицинские услуги",
      "id":"74"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Водные виды спорта (немоторизированные)",
      "id":"79"
    },
    {
      "groupName":"Основное",
      "name":"Библиотека",
      "id":"81"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Бассейн с подогревом",
      "id":"86"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Казино",
      "id":"93"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Игровая комната",
      "id":"98"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Бильярд",
      "id":"101"
    },
    {
      "groupName":"Основное",
      "name":"Бесплатный напиток (welcome drink)",
      "id":"106"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Дайвинг",
      "id":"113"
    },
    {
      "groupName":"Основное",
      "name":"Сувенирная лавка",
      "id":"118"
    },
    {
      "groupName":"Основное",
      "name":"Доступ для инвалидных колясок",
      "id":"120"
    },
    {
      "groupName":"В номере",
      "name":"Номер для инвалидов",
      "id":"125"
    },
    {
      "groupName":"В номере",
      "name":"Курящие номера",
      "id":"132"
    },
    {
      "groupName":"Персонал говорит на",
      "name":"Французский",
      "id":"137"
    },
    {
      "groupName":"Услуги",
      "name":"Бизнес-центр",
      "id":"6"
    },
    {
      "groupName":"Услуги",
      "name":"Прачечная",
      "id":"13"
    },
    {
      "groupName":"В номере",
      "name":"Радио",
      "id":"18"
    },
    {
      "groupName":"Основное",
      "name":"Переговорная комната",
      "id":"20"
    },
    {
      "groupName":"Услуги",
      "name":"Обслуживание в номерах",
      "id":"25"
    },
    {
      "groupName":"В номере",
      "name":"Сейф в номере",
      "id":"32"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Открытый бассейн",
      "id":"37"
    },
    {
      "groupName":"Услуги",
      "name":"Услуги консьержа",
      "id":"44"
    },
    {
      "groupName":"Услуги",
      "name":"Трансфер до аэропорта/отеля",
      "id":"49"
    },
    {
      "groupName":"В номере",
      "name":"Голосовая почта",
      "id":"51"
    },
    {
      "groupName":"Парковка",
      "name":"Парковка для автомобилей",
      "id":"56"
    },
    {
      "groupName":"Услуги",
      "name":"Няня",
      "id":"63"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Крытый бассейн",
      "id":"68"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Гольф-поле",
      "id":"70"
    },
    {
      "groupName":"Основное",
      "name":"Многоязычный персонал",
      "id":"75"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Оздоровительные процедуры",
      "id":"82"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Детский бассейн",
      "id":"87"
    },
    {
      "groupName":"Услуги",
      "name":"Салон красоты",
      "id":"94"
    },
    {
      "groupName":"В номере",
      "name":"Видео/DVD плеер",
      "id":"99"
    },
    {
      "groupName":"Основное",
      "name":"Пляж",
      "id":"102"
    },
    {
      "groupName":"Основное",
      "name":"Гей-френдли",
      "id":"107"
    },
    {
      "groupName":"Основное",
      "name":"Минимаркет",
      "id":"114"
    },
    {
      "groupName":"Основное",
      "name":"Эко-отель",
      "id":"119"
    },
    {
      "groupName":"Основное",
      "name":"Охрана",
      "id":"121"
    },
    {
      "groupName":"Услуги",
      "name":"Камера служба",
      "id":"126"
    },
    {
      "groupName":"В номере",
      "name":"Wi-Fi в номере",
      "id":"133"
    },
    {
      "groupName":"Персонал говорит на",
      "name":"Немецкий",
      "id":"138"
    },
    {
    "groupName":"Персонал говорит на",
    "name":"Арабский",
    "id":"140"
    },
    {
    "groupName":"В номере",
    "name":"Фен",
    "id":"2"
    },
    {
    "groupName":"В номере",
    "name":"Душ",
    "id":"7"
    },
    {
      "groupName":"Основное",
      "name":"Бар",
      "id":"14"
    },
    {
      "groupName":"В номере",
      "name":"Стол",
      "id":"19"
    },
    {
      "groupName":"Основное",
      "name":"Лифт",
      "id":"21"
    },
    {
      "groupName":"В номере",
      "name":"Ванна",
      "id":"26"
    },
    {
      "groupName":"В номере",
      "name":"Балкон/Терраса",
      "id":"33"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Бассейн",
      "id":"38"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Тренажерный зал",
      "id":"40"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Экскурсии",
      "id":"45"
    },
    {
      "groupName":"Основное",
      "name":"Лобби",
      "id":"52"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Джакузи",
      "id":"57"
    },
    {
      "groupName":"Основное",
      "name":"Банкетный зал",
      "id":"64"
    },
    {
      "groupName":"Услуги",
      "name":"Обмен валюты",
      "id":"69"
    },
    {
      "groupName":"В номере",
      "name":"Электронные ключи",
      "id":"71"
    },
    {
      "groupName":"Основное",
      "name":"Зонтики",
      "id":"76"
    },
    {
      "groupName":"Интернет",
      "name":"Wi-Fi",
      "id":"83"
    },
    {
      "groupName":"Услуги",
      "name":"Завтрак на вынос",
      "id":"88"
    },
    {
      "groupName":"Основное",
      "name":"Сауна/Парная",
      "id":"95"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Сквош-корт",
      "id":"103"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Водные виды спорта (моторизированные)",
      "id":"108"
    },
    {
      "groupName":"В номере",
      "name":"Тапочки",
      "id":"110"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Мини-гольф",
      "id":"115"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Развлечения для детей",
      "id":"122"
    },
    {
      "groupName":"Услуги",
      "name":"Копировальные услуги",
      "id":"127"
    },
    {
      "groupName":"В номере",
      "name":"Ежедневная уборка",
      "id":"134"
    },
    {
      "groupName":"Персонал говорит на",
      "name":"Испанский",
      "id":"139"
    },
    {
      "groupName":"Персонал говорит на",
      "name":"Итальянский",
      "id":"141"
    },
    {
      "groupName":"В номере",
      "name":"Сейф",
      "id":"3"
    },
    {
      "groupName":"В номере",
      "name":"Номера для некурящих",
      "id":"8"
    },
    {
      "groupName":"В номере",
      "name":"Отдельный душ и ванна",
      "id":"10"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Сауна",
      "id":"15"
    },
    {
      "groupName":"В номере",
      "name":"Ванная",
      "id":"22"
    },
    {
      "groupName":"В номере",
      "name":"Кофеварка/Чайник",
      "id":"27"
    },
    {
      "groupName":"Основное",
      "name":"Кафе",
      "id":"41"
    },
    {
      "groupName":"Основное",
      "name":"Конференц-зал",
      "id":"46"
    },
    {
      "groupName":"В номере",
      "name":"Кухня",
      "id":"53"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Прокат велосипедов",
      "id":"58"
    },
    {
      "groupName":"В номере",
      "name":"Микроволновая печь",
      "id":"60"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Спа",
      "id":"65"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Солярий",
      "id":"72"
    },
    {
      "groupName":"Основное",
      "name":"Камера хранения",
      "id":"77"
    },
    {
      "groupName":"Основное",
      "name":"Гардероб",
      "id":"84"
    },
    {
      "groupName":"Основное",
      "name":"Прачечная",
      "id":"89"
    },
    {
      "groupName":"Основное",
      "name":"Стиральная машина",
      "id":"91"
    },
    {
      "groupName":"Парковка",
      "name":"Прокат автомобилей в отеле",
      "id":"96"
    },
    {
      "groupName":"Услуги",
      "name":"Услуги секретаря",
      "id":"104"
    },
    {
      "groupName":"Парковка",
      "name":"Гараж",
      "id":"109"
    },
    {
      "groupName":"Парковка",
      "name":"Услуги парковки",
      "id":"111"
    },
    {
      "groupName":"Спорт и отдых",
      "name":"Боулинг",
      "id":"116"
    },
    {
      "groupName":"Основное",
      "name":"Внутренний кино-канал",
      "id":"123"
    },
    {
      "groupName":"Услуги",
      "name":"Портье",
      "id":"128"
    },
    {
      "groupName":"Основное",
      "name":"Экскурсионное бюро",
      "id":"130"
    },
    {
      "groupName":"В номере",
      "name":"Смежные номера",
      "id":"135"
    },
    {
      "groupName":"Персонал говорит на",
      "name":"Китайский",
      "id":"142"
    }
  ]
};

const amenities = {};

data.amenities.forEach(item => {
  amenities[item.id] = {
    name: item.name,
    group: item.groupName
  };
});

data.amenities = amenities;

export default data;